import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title1 = 'Hotel Room Service';
  title2 = 'Hotel - Motel - Holiday Inn';
}

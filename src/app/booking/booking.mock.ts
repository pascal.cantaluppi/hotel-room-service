import { Booking } from './booking.interface';

export const bookings: Booking[] = [
  {
    id: 1,
    name: 'John Doe',
    room: '101',
    checkin: new Date(),
    checkout: new Date(),
    adults: 2,
    children: 0,
    status: 1,
  },
  {
    id: 2,
    name: 'Fred Flintstone',
    room: '102',
    checkin: new Date(),
    checkout: new Date(),
    adults: 2,
    children: 2,
    status: 1,
  },
  {
    id: 3,
    name: 'Jane Doe',
    room: '104',
    checkin: new Date(),
    checkout: new Date(),
    adults: 2,
    children: 2,
    status: 1,
  },
];

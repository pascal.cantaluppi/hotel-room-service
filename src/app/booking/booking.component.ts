import { Component, OnInit } from '@angular/core';
import { Booking } from './booking.interface';
import { bookings } from './booking.mock';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],
})
export class BookingComponent implements OnInit {
  constructor() {}

  bookings: Booking[] = bookings;

  ngOnInit(): void {}
}

export interface Booking {
  id: number;
  name: string;
  room: string;
  checkin: Date;
  checkout: Date;
  adults: number;
  children: number;
  status: number;
}
